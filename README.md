# Natural Computing Assignment 6: Swarm Intelligence

Please note that our source code is an augmentation of [Christoph Schmidtmeier's Boids](https://gitlab.com/chrismit3s/boids).

Running instructions: Obtain dependencies from `pyproject.toml` and run `src/main.py` with Python. The visualisation can only be escaped using the escape key, upon which it is possible to save the file.
