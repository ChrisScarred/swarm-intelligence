from typing import Tuple
import numpy as np


def convert_colour(hex: str) -> Tuple[int, int, int]:
    hex = hex.strip("#")
    rgb = [int(hex[i : i + 2], 16) for i in (0, 2, 4)]
    r, g, b = rgb
    return tuple(np.array([b, g, r]).tolist())