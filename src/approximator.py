import random
from typing import Callable, Dict, List, Optional, Tuple

import numpy as np
from simulation import Simulation


class BoidsApproximator:
    def __init__(
        self, distance_function: Callable, default_simulation_config: Dict
    ) -> None:
        self.distance = distance_function
        self.config = default_simulation_config

    def get_positions(self) -> Tuple[List[np.ndarray], List[float]]:
        size = self.config.get("env_size")
        boids = self.config.get("n")
        positions = [
            np.array([random.randrange(0, size[0]), random.randrange(0, size[1])])
            for _ in range(boids)
        ]
        angles = [np.random.random() * np.pi for _ in range(boids)]
        return positions, angles

    def get_epsilons(self, target_epsilon: float, steps: int):
        epsilons = [target_epsilon]
        current_epsilon = target_epsilon
        for _ in range(steps):
            new_epsilon = current_epsilon * 2
            epsilons.append(new_epsilon)
            current_epsilon = new_epsilon
        return epsilons

    def sample(
        self,
        priors: Optional[Dict[str, Callable]],
        previous_population: Optional[List[Tuple[float, float, float]]],
    ) -> Tuple[float, float, float]:
        if isinstance(priors, Dict):
            w_s_prior = priors.get("w_s")
            w_a_prior = priors.get("w_a")
            w_c_prior = priors.get("w_c")
            return w_s_prior(), w_a_prior(), w_c_prior()
        if isinstance(previous_population, List):
            ws = [p[0] for p in previous_population]
            wa = [p[1] for p in previous_population]
            wc = [p[2] for p in previous_population]
            return (random.choice(ws), random.choice(wa), random.choice(wc))

    def get_new_population(
        self,
        init_positions: Tuple,
        sim_steps: int,
        sim: Simulation,
        previous_population: Optional[List[Tuple[float, float, float]]],
        priors: Optional[Dict[str, Callable]],
        epsilon: float,
        target_size: int,
        mutation_mean: float,
        mutation_std: float,
    ) -> Tuple[List[Tuple[float, float, float]], List[float]]:
        mutation = lambda: random.choice([1, -1]) * np.random.normal(
            loc=mutation_mean, scale=mutation_std
        )
        population = []
        distances = []
        current_size = 0
        while current_size < target_size:
            ws = 0
            wa = 0
            wc = 0
            distance = 100
            d = 100
            while distance > epsilon:
                ws, wa, wc = self.sample(priors, previous_population)
                ws += mutation()
                wa += mutation()
                wc += mutation()
                data = sim.run(
                    steps=sim_steps,
                    initial_positions=init_positions,
                    save_data=False,
                    w_s=ws,
                    w_a=wa,
                    w_c=wc,
                    visualise=False,
                )
                d = self.distance(data)
                distance = d
            population.append((ws, wa, wc))
            distances.append(d)
            current_size += 1
        return population, distances

    def run(
        self,
        n: int,
        target_epsilon: float,
        priors: Dict[str, Callable],
        sim_steps: int,
        approximator_steps: int,
        mutation_mean: float = 0.05,
        mutation_std: float = 0.02,
    ) -> None:

        epsilons = sorted(
            self.get_epsilons(target_epsilon, approximator_steps), reverse=True
        )
        populations = []
        distances = []

        sim = Simulation(self.config)

        for i in range(approximator_steps):
            # get initial positions and angles for the boids so:
            # 1) the initial positions are sampled randomly from the uniform distribution
            # 2) the inital conditions are different every iteration
            # 3) the initial conditions remain the same within one iteration
            # this should lead to a fair evaluation of 'fitness' during an iteration
            # while preventing overfitting on a fixed initial positions set
            positions = self.get_positions()
            population = []
            distance = []
            if i == 0:
                population, distance = self.get_new_population(
                    positions,
                    sim_steps,
                    sim,
                    None,
                    priors,
                    epsilons[i],
                    n,
                    mutation_mean,
                    mutation_std,
                )
            else:
                population, distance = self.get_new_population(
                    positions,
                    sim_steps,
                    sim,
                    populations[-1],
                    None,
                    epsilons[i],
                    n,
                    mutation_mean,
                    mutation_std,
                )
            print(f"Generation {i + 1}/{approximator_steps}")
            populations.append(population)
            distances.append(distance)
        return populations, distances
