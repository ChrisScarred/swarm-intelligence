import random
from typing import List, Optional, Tuple

import numpy as np
from boid import Boid
from canvas import Canvas

MIN = 10 ** (-15)


def _angle(x):
    return np.arctan2(x[1], x[0])


def _norm(x):
    return x if np.allclose(x, 0) else x / max(np.linalg.norm(x), MIN)


class Universe:
    def __init__(
        self,
        view_dist,
        edge_behaviour,
        sep,
        align,
        cohes,
        palette,
        velocity,
        turn_speed,
        nose_length,
        size,
        dt,
    ):
        self.boids = []
        self.view_dist = view_dist
        self.edge_behaviour = edge_behaviour
        self.weights = {"seperation": sep, "alignment": align, "cohesion": cohes}
        self.palette = palette
        self.velocity = velocity
        self.turn_speed = turn_speed
        self.nose_length = nose_length
        self.size = size
        self.dt = dt

    def add_boid(self, color=None, pos=None, angle=None):
        color = color or random.choice(self.palette["accents"])
        pos = (
            pos
            if pos is not None
            else self.size * (1 - 2 * np.random.random(self.size.shape))
        )
        angle = angle or (2 * np.pi * np.random.random())
        self.boids.append(
            Boid(color, pos, angle, self.velocity, self.turn_speed, self.nose_length)
        )

    def populate(
        self, n, inital_positions: Optional[Tuple[List[np.ndarray], List[float]]]
    ):
        init = False
        if inital_positions:
            positions, angles = inital_positions
            if len(positions) >= n and len(angles) >= n:
                for i in range(n):
                    self.add_boid(pos=positions[i], angle=angles[i])
                init = True
        if not init:
            for _ in range(n):
                self.add_boid()

    def get_nearby(self, boid):
        out = []
        for other in self.boids:
            if boid.dist(other.pos) < self.view_dist and boid is not other:
                out.append(other)
        return out

    def reorient(self, boid):
        """
        calculates the new direction of the boid with 3 rules: cohesion,
        seperation, alignment
        """
        # get nearby boids
        nearby = self.get_nearby(boid)

        avg_pos = np.array((0, 0), dtype="float")  # cohesion
        avg_dir = np.array((0, 0), dtype="float")  # alignment
        avoid_boids = np.array((0, 0), dtype="float")  # seperation
        avoid_walls = np.array(
            (0, 0), dtype="float"
        )  # turn away from walls (if enabled)

        # calculate all three forces if there are any boids nearby
        if len(nearby) != 0:
            for i, other in enumerate(nearby):
                diff = other.pos - boid.pos

                avg_pos += (diff - avg_pos) / (i + 1)  # running average
                avg_dir += (other.dir - avg_dir) / (i + 1)  # running average
                avoid_boids -= diff / max(np.dot(diff, diff), MIN)

            # normalize them
            avg_pos = _norm(avg_pos)
            avg_dir = _norm(avg_dir)
            avoid_boids = _norm(avoid_boids)

        # if an edge is in view range
        if (
            self.edge_behaviour == "avoid"
            and (np.abs(boid.pos) > self.size - self.view_dist).any()
        ):
            for i, (coord, lower, upper) in enumerate(
                zip(boid.pos, -self.size, self.size)
            ):
                if (diff := coord - lower) < self.view_dist:
                    avoid_walls[i] += np.abs(1 / max(diff, MIN))
                if (diff := upper - coord) < self.view_dist:
                    avoid_walls[i] -= np.abs(1 / max(diff, MIN))

        # sum them up and if its not zero return it
        sum = _norm(avoid_walls) + _norm(
            self.weights["seperation"] * avoid_boids
            + self.weights["cohesion"] * avg_pos
            + self.weights["alignment"] * avg_dir
        )
        if np.allclose(sum, 0):
            return boid.angle
        else:
            return _angle(sum)

    def draw(self, canvas: Canvas, stop: bool):
        canvas.fill(self.palette["background"])
        for boid in self.boids:
            boid.draw(canvas)
        canvas.update(stop)

    def get_nearest_distance(self, boid: Boid) -> float:
        nearest_distance = np.max(self.size * 2)
        for b in self.boids:
            if b != boid:
                d = boid.dist(b.pos)
                if d < nearest_distance:
                    nearest_distance = d
        return nearest_distance

    def get_order_param(self) -> float:
        factor = 1 / len(self.boids)
        sum_ = np.sum([b.vel / max(np.linalg.norm(b.vel), MIN) for b in self.boids])
        return factor * np.linalg.norm(sum_)

    def tick(self, record_data: bool, record_metrics: List[str]):
        angles = []
        nearest_distances = []

        for boid in self.boids:
            if record_data and "nnd" in record_metrics:
                nearest_distances.append(self.get_nearest_distance(boid))
            angles.append(self.reorient(boid))

        for boid, angle in zip(self.boids, angles):
            if self.edge_behaviour == "wrap":
                self.wrap(boid)
            boid.turn_to(angle, self.dt)
            boid.tick(self.dt)

        recorded_data = None
        if record_data:
            if "order" in record_metrics:
                recorded_data = (nearest_distances, self.get_order_param())
            else:
                recorded_data = (nearest_distances, None)

        return record_data, recorded_data

    def wrap(self, boid):
        boid.pos = (boid.pos + self.size) % (2 * self.size) - self.size

    def loop(
        self,
        rt: int,
        max_t: Optional[int],
        canvas: Optional[Canvas] = None,
        record_metrics: List[str] = [],
    ):
        t = 0
        orders = []
        distances = []
        if canvas:
            while canvas.is_open():
                self.draw(canvas, t > max_t if max_t else False)
                recorded, data = self.tick(t < rt, record_metrics)
                if recorded:
                    d, o = data
                    if o is not None:
                        orders.append(o)
                    if len(d) > 0:
                        distances.append(d)
                t += 1
        else:
            for _ in range(max_t if max_t else rt):
                recorded, data = self.tick(t < rt, record_metrics)
                if recorded:
                    d, o = data
                    if o is not None:
                        orders.append(o)
                    if len(d) > 0:
                        distances.append(d)
                t += 1
        return orders, distances
