import os
from typing import Any, Dict, List, Optional, Tuple, Union

import numpy as np

from canvas import Canvas
from universe import Universe


class Simulation:
    def __init__(self, default_params: Dict[str, Union[Dict[str, Any], Any]]) -> None:
        self.env_size = default_params.get("env_size", (100, 100))
        self.fps = default_params.get("fps", 60)
        self.data_dir = default_params.get("data_dir", "data")
        self.scale = default_params.get("scale", 1.0)
        self.view_distance = default_params.get("view_distance", 80.0)
        self.edge_behaviour = default_params.get("edge_behaviour", "avoid")
        self.w_s = default_params.get("w_s", 1.0)
        self.w_a = default_params.get("w_a", 1.0)
        self.w_c = default_params.get("w_c", 1.0)
        self.palette = default_params.get(
            "palette",
            {
                "background": (0x2A, 0x18, 0x0B),
                "highlight": (0x60, 0x70, 0xF8),
                "accents": [(0xC2, 0xC7, 0xEF), (0xD6, 0xD7, 0xCD), (0xD4, 0xE5, 0xFF)],
            },
        )
        self.velocity = default_params.get("velocity", 90)
        self.turn_speed = default_params.get("turn_speed", 3.6)
        self.nose_length = default_params.get("nose_length", 10)
        self.n = default_params.get("n", 60)
        self.highlight = default_params.get("highlight", False)
        self.data_recording_steps = default_params.get("data_recording_steps", 500)
        self.visualise = default_params.get("visualise", True)
        self.fname = default_params.get("fname", "out.csv")
        self.metrics = default_params.get("record_metrics", [])

    def _parse_kwargs(self, kwargs: Dict[str, Union[Dict[str, Any], Any]]) -> Tuple:
        return (
            kwargs.get("env_size", self.env_size),
            kwargs.get("fps", self.fps),
            kwargs.get("data_dir", self.data_dir),
            kwargs.get("scale", self.scale),
            kwargs.get("view_distance", self.view_distance),
            kwargs.get("edge_behaviour", self.edge_behaviour),
            kwargs.get("w_s", self.w_s),
            kwargs.get("w_a", self.w_a),
            kwargs.get("w_c", self.w_c),
            kwargs.get("palette", self.palette),
            kwargs.get("velocity", self.velocity),
            kwargs.get("turn_speed", self.turn_speed),
            kwargs.get("nose_length", self.nose_length),
            kwargs.get("n", self.n),
            kwargs.get("highlight", self.highlight),
            kwargs.get("data_recording_steps", self.data_recording_steps),
            kwargs.get("visualise", self.visualise),
            kwargs.get("fname", self.fname),
            kwargs.get("record_metrics", self.metrics),
        )

    def save_data(
        self,
        orders: List[float],
        distances: List[List[float]],
        data_dir: str,
        fname: str,
    ):
        full_path = os.path.join(data_dir, fname)
        with open(full_path, "w") as f:
            header = "t"
            if len(orders) > 0:
                header += ",order"
            if len(distances) > 0:
                header += ","
                header += ",".join([str(i) for i in range(len(distances[0]))])
            header += "\n"
            f.write(header)
            if len(orders) > 0 and len(distances) > 0:
                for t, (order, ds) in enumerate(zip(orders, distances)):
                    ds = [str(i) for i in ds]
                    f.write(f"{t},{order},{','.join(ds)}\n")
            elif len(orders) > 0:
                f.writelines([f"{t},{order}" for t, order in enumerate(orders)])
            elif len(distances) > 0:
                f.writelines(
                    [
                        f"{t},{','.join([str(i) for i in ds])}"
                        for t, ds in enumerate(distances)
                    ]
                )

    def run_with_visuals(
        self,
        env_size,
        fps,
        data_dir,
        scale,
        view_distance,
        edge_behaviour,
        w_s,
        w_a,
        w_c,
        palette,
        velocity,
        turn_speed,
        nose_length,
        n,
        highlight,
        data_recording_steps,
        fname,
        record_metrics,
        steps,
        initial_positions,
        save_data: bool,
    ) -> Tuple[List[int], List[np.ndarray]]:
        orders = []
        distances = []
        canvas = Canvas(env_size, fps, data_dir, scale)
        size = canvas.size
        with canvas:
            u = Universe(
                view_distance,
                edge_behaviour,
                w_s,
                w_a,
                w_c,
                palette,
                velocity,
                turn_speed,
                nose_length,
                size,
                1 / fps,
            )
            if highlight:
                u.add_boid(color=palette.get("highlight"), pos=(0, 0))
                n -= 1

            u.populate(n, initial_positions)
            orders, distances = u.loop(
                data_recording_steps,
                steps,
                canvas=canvas,
                record_metrics=record_metrics,
            )

        if input("Save video? (y/N) ").lower() != "y":
            os.remove(canvas.filename)

        if save_data:
            if len(orders) > 0 or len(distances) > 0:
                self.save_data(orders, distances, data_dir, fname)
        return orders, distances

    def run_without_visuals(
        self,
        env_size,
        fps,
        data_dir,
        scale,
        view_distance,
        edge_behaviour,
        w_s,
        w_a,
        w_c,
        palette,
        velocity,
        turn_speed,
        nose_length,
        n,
        data_recording_steps,
        fname,
        record_metrics,
        steps,
        initial_positions,
        save_data: bool,
    ) -> Tuple[List[int], List[np.ndarray]]:
        orders = []
        distances = []
        canvas = Canvas(env_size, fps, data_dir, scale)
        size = canvas.size
        os.remove(canvas.filename)
        u = Universe(
            view_distance,
            edge_behaviour,
            w_s,
            w_a,
            w_c,
            palette,
            velocity,
            turn_speed,
            nose_length,
            size,
            1 / fps,
        )

        u.populate(n, initial_positions)
        orders, distances = u.loop(
            data_recording_steps, steps, record_metrics=record_metrics
        )
        if save_data:
            if len(orders) > 0 or len(distances) > 0:
                self.save_data(orders, distances, data_dir, fname)
        return orders, distances

    def run(
        self,
        steps: Optional[int] = None,
        initial_positions: Optional[Tuple[List[np.ndarray], List[float]]] = None,
        save_data: bool = True,
        **kwargs,
    ) -> Tuple[List[int], List[np.ndarray]]:
        (
            env_size,
            fps,
            data_dir,
            scale,
            view_distance,
            edge_behaviour,
            w_s,
            w_a,
            w_c,
            palette,
            velocity,
            turn_speed,
            nose_length,
            n,
            highlight,
            data_recording_steps,
            visualise,
            fname,
            record,
        ) = self._parse_kwargs(kwargs)
        if visualise:
            return self.run_with_visuals(
                env_size,
                fps,
                data_dir,
                scale,
                view_distance,
                edge_behaviour,
                w_s,
                w_a,
                w_c,
                palette,
                velocity,
                turn_speed,
                nose_length,
                n,
                highlight,
                data_recording_steps,
                fname,
                record,
                steps,
                initial_positions,
                save_data,
            )
        return self.run_without_visuals(
                env_size,
                fps,
                data_dir,
                scale,
                view_distance,
                edge_behaviour,
                w_s,
                w_a,
                w_c,
                palette,
                velocity,
                turn_speed,
                nose_length,
                n,
                data_recording_steps,
                fname,
                record,
                steps,
                initial_positions,
                save_data,
            )
