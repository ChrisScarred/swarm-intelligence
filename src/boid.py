import numpy as np


def _unit_vector(angle):
    return np.array([np.cos(angle), np.sin(angle)], dtype="float")


class Boid:
    def __init__(self, color, pos, angle, velocity, turn_speed, nose_length):
        self.pos = np.array(pos, dtype="float")
        self.angle = angle % (2 * np.pi)
        self.color = color
        self.velocity = velocity
        self.turn_speed = turn_speed
        self.nose_length = nose_length

    @property
    def dir(self):
        return _unit_vector(self.angle)

    @property
    def vel(self):
        return self.velocity * self.dir

    def dist(self, pos):
        return np.linalg.norm(self.pos - pos)

    def turn_by(self, dangle, dt):
        # dont turn too fast
        self.angle += np.clip(dangle, -dt * self.turn_speed, dt * self.turn_speed)

        # keep angle in range [0, 2pi)
        self.angle %= 2 * np.pi

    def turn_to(self, angle, dt):
        a = (angle - self.angle) % (2 * np.pi)
        b = -(-a % (2 * np.pi))
        self.turn_by(min(a, b, key=lambda x: np.abs(x)), dt)

    def draw(self, canvas):
        tip = self.pos + self.nose_length * self.dir
        left = self.pos + self.nose_length / 2 * _unit_vector(
            self.angle + 2 * np.pi / 3
        )
        right = self.pos + self.nose_length / 2 * _unit_vector(
            self.angle - 2 * np.pi / 3
        )
        canvas.draw_poly([tip, left, self.pos, right], self.color)

    def tick(self, dt):
        self.pos += self.vel * dt
