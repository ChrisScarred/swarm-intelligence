import pickle
from textwrap import wrap
from typing import Dict, List, Tuple

import matplotlib.pyplot as plt
import numpy as np

import pandas as pd
import seaborn as sns

from PIL import Image
from simulation import Simulation


def multi_target_seq_abc_pickles_to_df(
    fpaths: Dict[str, List[Dict[float, str]]],
    n_targets: int = 2,
    n_iters: int = 10,
    population_size: int = 20,
) -> None:
    targets = list(fpaths.get("populations", {}).keys())
    df = pd.DataFrame(
        columns=[
            "Target Order",
            "Generation",
            "Subject",
            "Parameter w_s",
            "Parameter w_a",
            "Parameter w_c",
            "Distance from Target",
        ],
        index=[i for i in range(n_targets * n_iters * population_size)],
    )
    i = 0
    for target in targets:
        populations = []
        distances = []
        with open(fpaths.get("populations", {}).get(target), "rb") as f:
            populations = pickle.load(f)
        with open(fpaths.get("distances", {}).get(target), "rb") as f:
            distances = pickle.load(f)
        for gen in range(n_iters):
            for subject in range(population_size):
                ws, wa, wc = populations[gen][subject]
                distance = distances[gen][subject]
                df.loc[i] = pd.Series(
                    {
                        "Target Order": target,
                        "Generation": gen,
                        "Subject": subject,
                        "Parameter w_s": ws,
                        "Parameter w_a": wa,
                        "Parameter w_c": wc,
                        "Distance from Target": distance,
                    }
                )
                i += 1
    df.to_csv("data/abc_data.csv")


def plot_distros(df: pd.DataFrame, target: float, gens: List[float]) -> None:
    df = df.loc[df["Target Order"] == target]
    fig, ax = plt.subplots(len(gens), 4, sharey=True, figsize=(9, 60))
    fig.set_layout_engine("tight")
    fig.suptitle(_get_title(f"Marginal Distributions at Target Order = {target}"))
    for i in gens:
        i = int(i)
        for j, p in enumerate(
            ["Parameter w_a", "Parameter w_s", "Parameter w_c", "Distance from Target"]
        ):
            subset = df.loc[df["Generation"] == i]
            ax[i][j].set_xticks([])
            ax[i][j].set_yticks([])
            plt.setp(ax[i][j].spines.values(), alpha=0)
            if j == 3:
                a = sns.displot(data=subset, x=p, kde=True, binrange=(0, 1))
            else:
                a = sns.displot(data=subset, x=p, kde=True, binrange=(0, 10))
            plt.close()
            if i == 0:
                ax[i][j].set_title(_get_title(p, max_line_len=20))
            if j == 0:
                ax[i][j].set_ylabel(f"Generation {i+1}")
            path = f"figures/components/{target}_{i}_{j}.png"
            a.savefig(path)
            ax[i][j].imshow(
                Image.open(path),
            )
    fig.savefig(f"figures/parameter_distributions_target_order_{target}.png", dpi=600)


def _get_title(title: str, max_line_len: int = 60) -> str:
    return "\n".join(wrap(title, max_line_len))


def visualise_multi_target_seq_abc(fpath: str) -> None:
    df = pd.read_csv(fpath)
    gens = np.unique(df["Generation"].to_numpy())
    targets = np.unique(df["Target Order"].to_numpy())

    for t in targets:
        plot_distros(df, t, gens)


def alignment_over_time(in_fname: str, out_fname: str, params: str, which: List[str]) -> None:
    df = pd.read_csv(in_fname)
    df["avg_dist"] = df.apply(
        lambda x: np.mean(np.array([x[f"{i}"] for i in range(15)])), axis=1
    )
    fig, ax = plt.subplots(1, 1)

    tit = "Order Parameter"
    if len(which) > 1:
        tit = "Performance"
    elif "nnd" in which:
        tit = "Mean Distance to the Nearest Neighbour"
    fig.suptitle(_get_title(f"{tit} over Time with Parameters {params}"))

    if "order" in which:
        ax.plot(df["t"], df["order"], label="The Order Parameter")
    if "nnd" in which:
        ax.plot(df["t"], df["avg_dist"], label="The Mean Distance to the Nearest Neighbour")
    if len(which) > 1:
        ax.legend()
    ax.set_xlabel("Time in Steps")

    ylabel = "Value of Order Parameter"
    if len(which) > 1:
        ylabel = "Value of Metrics"
    elif "nnd" in which:
        ylabel = "Mean Distance to the Nearest Neighbour"
    ax.set_ylabel(ylabel)
    fig.savefig(out_fname)


def multiple_alignments_with_sim(
    config: Dict, population: List[Tuple[float, float, float]]
) -> None:
    sim = Simulation(config)
    for ws, wa, wc in population:
        fname = f"{str(ws).replace('.', '-')}_{str(wa).replace('.', '-')}_{str(wc).replace('.', '-')}"
        sim.run(fname=f"{fname}.csv", w_s=ws, w_a=wa, w_c=wc, visualise=False)
        alignment_over_time(
            f"data/{fname}.csv",
            f"figures/alignment_nnd_{fname}.png",
            f"w_s={ws}, w_a={wa}, w_c={wc}",
            which=["order", "nnd"]
        )
        alignment_over_time(
            f"data/{fname}.csv",
            f"figures/alignment_{fname}.png",
            f"w_s={ws}, w_a={wa}, w_c={wc}",
            which=["order"]
        )
        alignment_over_time(
            f"data/{fname}.csv",
            f"figures/nnd_{fname}.png",
            f"w_s={ws}, w_a={wa}, w_c={wc}",
            which=["nnd"]
        )