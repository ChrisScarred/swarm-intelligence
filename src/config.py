from utils import convert_colour

CONFIG = {
    "n": 15,  # number of boids
    "env_size": (100, 100),  # in cartesian coordinates
    "view_distance": 15.0,  # distance the boid sees in cartesian coordinates
    "velocity": 70.0,  # distance in cartesian coordinates per second
    "nose_length": 10,  # in cartesian coordinates
    "turn_speed": 7.2,  # in rads per seconds
    "data_dir": "data",
    # scaling of cartesian coordinates to pixels; e.g., if env_size is (100, 100)
    # and scale is 5.0, the video is 500x500 px
    "scale": 5.0,
    "palette": {
        "background": convert_colour("#0b182a"),
        "highlight": convert_colour("#f87060"),
        "accents": [
            convert_colour("#cdd7d6"),
            convert_colour("#efc7c2"),
            convert_colour("#ffe5d4"),
        ],
    },
    "fps": 60,  # desired fps, causes the dt to be 1 / fps
    "edge_behaviour": "avoid",  # "avoid" or "wrap"
    "w_s": 1.0,  # weight separation
    "w_a": 1.0,  # weight alignment
    "w_c": 1.0,  # weight cohesion
    # the list of metrics to record. Valid entries are "order" for the order parameter
    # and "nnd" for the nearest-neighbour distance
    "record_metrics": ["order", "nnd"],
    # the first x steps for which to record the order parameter and the nearest-neighbour distance
    "data_recording_steps": 300,
    # whether to show video of the simulation
    "visualise": True,
    # default file name for storing recorded metrics
    "fname": "out.csv",
}
