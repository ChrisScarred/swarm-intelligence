import pickle
from typing import Any, Dict, List, Optional, Tuple, Union

import numpy as np
from analysis import (
    alignment_over_time,
    multi_target_seq_abc_pickles_to_df,
    visualise_multi_target_seq_abc,
    multiple_alignments_with_sim
)
from approximator import BoidsApproximator
from config import CONFIG
from simulation import Simulation


def run_single_simulation(config: Dict[str, Union[Dict[str, Any], Any]]) -> None:
    sim = Simulation(config)
    sim.run(fname="default_params.csv", visualise=False)

    alignment_over_time(
        "data/default_params.csv",
        "figures/alignment_default_params.png",
        "w_s=1, w_a=1, w_c=1",
        which=["order", "nnd"]
    )
    alignment_over_time(
        "data/default_params.csv",
        "figures/alignment_default_params.png",
        "w_s=1, w_a=1, w_c=1",
        which=["order"]
    )
    alignment_over_time(
        "data/default_params.csv",
        "figures/alignment_default_params.png",
        "w_s=1, w_a=1, w_c=1",
        which=["nnd"]
    )


def run_sequential_abc(
    config: Dict[str, Union[Dict[str, Any], Any]], target: float
) -> None:
    approx = BoidsApproximator(lambda x: abs(target - x[0][-1]), config)
    prior = lambda: np.random.uniform(0.0, 10.0)
    populations, distances = approx.run(
        20, 0.05, {"w_s": prior, "w_a": prior, "w_c": prior}, 300, 10
    )
    target = int(target)
    with open(f"data/populations_{target}.pickle", "wb") as f:
        pickle.dump(populations, f)
    with open(f"data/distances_{target}.pickle", "wb") as f:
        pickle.dump(distances, f)


def seq_abc_data_pickles_to_df(fpaths: Optional[Dict[str, List[str]]] = None) -> None:
    if not isinstance(fpaths, Dict):
        fpaths = {
            "populations": {
                j: f"data/populations_{i}.pickle" for i, j in [(0, 0.6), (1, 1.0)]
            },
            "distances": {
                j: f"data/distances_{i}.pickle" for i, j in [(0, 0.6), (1, 1.0)]
            },
        }
    multi_target_seq_abc_pickles_to_df(fpaths)


def analyse_seq_abc_df(fpath: Optional[str] = None) -> None:
    if not isinstance(fpath, str):
        fpath = "data/abc_data.csv"
    visualise_multi_target_seq_abc(fpath)


def show_specific_abc_sims(
        config: Dict,
    selected: Optional[List[Tuple[float, float, float]]] = None
) -> None:
    if not selected:
        selected = [
            (8.15, 2.17, 1.6),
            (9.56, 2.12, 4.73),
            (-0.01, 2.43, 5.0),
            (0.67, 9.25, 7.14),
            (9.78, 9.30, 7.32),
            (9.51, 6.62, 5.78),
        ]
    multiple_alignments_with_sim(config, selected)

if __name__ == "__main__":
    from config import CONFIG

    run_single_simulation(CONFIG)
    # run_sequential_abc(CONFIG, 0.6)
    # seq_abc_data_pickles_to_df()
    # analyse_seq_abc_df()
    show_specific_abc_sims(CONFIG)
